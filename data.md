---
clavier: false
gestionGrosMots: true
rechercheContenu: false
footer: false
---
​
# Chatbot - TED-i BEAM
​
![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_71b9ea33de335b267b343dffe24d1be9.PNG)

Bonjour à tous et bienvenue dans l'assitant virtuel de la DRNE. Mon rôle est de vous aider à découvrir **le dispositif TED-i**. 

J'ai été élaboré à partir d'un outil libre et gratuit créé par [Cédric Eyssette](https://eyssette.forge.apps.education.fr/) - sources sur [la Forge](https://forge.apps.education.fr/chatMD/chatMD.forge.apps.education.fr).
_Appuyer sur "Entrée" pour stopper l'effet "machine à écrire" et afficher le texte immédiatement._


Je suis :
1. [ELEVE](ELEVE)
2. [ENSEIGNANT](ENSEIGNANT)
3. [DIRECTEUR ET CHEF D'ETABLISSEMENT](DIRECTEUR ET CHEF-D'ETABLISSEMENT)

## ENSEIGNANT
Je suis paramétré pour répondre aux questions suivantes :
1. [Qu'est ce que le dispositif TED-i ?](Qu'est ce que le dispositif TED-i ?)
2. [Quel matériel est mis à ma disposition en classe ?](Quel matériel est mis à ma disposition en classe ?)
3. [Quelles sont les bonnes pratiques à respecter ?](Quelles sont les bonnes pratiques à respecter ?)
4. [Comment mettre en fonctionnement le robot BEAM ?](Comment mettre en fonctionnement le robot BEAM ?)
5. [Comment mettre en veille le robot BEAM ?](Comment mettre en veille le robot BEAM ?)
6. [Que faire en cas de problème de connexion ?](Que faire en cas de problème de connexion ?)
7. [L'élève peut-il partager son travail via le robot ?](L'élève peut-il partager son travail via le robot ?)
8. [Comment utiliser le visualiseur ?](Comment utiliser le visualiseur ?)
9. [Comment l'élève peut-il me signaler qu'il souhaite prendre la parole ou qu'il a un problème ?](Comment l'élève peut-il me signaler qu'il souhaite prendre la parole ou qu'il a un problème ?)
10. [Changer de profil](Changer de profil)

## ELEVE
Je suis paramétré pour répondre aux questions suivantes :
1. [Quel matériel est mis à ma disposition ?](Quel matériel est mis à ma disposition ?)
2. [Puis-je utiliser mon ordinateur personnel ou une tablette ?](Puis-je utiliser mon ordinateur personnel ou une tablette ?)
3. [Comment me connecter à l'application Beam® ?](Comment me connecter à l'application Beam® ?)
4. [Comment utiliser l'application Beam® ?](Comment utiliser l'application Beam® ?)
5. [Que faire si je ne parviens pas à me connecter au robot ?](Que faire si je ne parviens pas à me connecter au robot ?)
6. [Puis-je consulter un document posé sur une table en classe?](Puis-je consulter un document posé sur une table en classe ?)
7. [Comment utiliser le visualiseur fixé sur le robot ?](Comment utiliser le visualiseur fixé sur le robot ?)
8. [Comment utiliser le partage d'écran pour montrer mon travail via le robot ?](Comment utiliser le partage d'écran pour montrer mon travail via le robot ?)
9. [Comment signaler à mon enseignant que je souhaite prendre la parole ou que j'ai un problème ?](Comment signaler à mon enseignant que je souhaite prendre la parole ou que j'ai un problème ?)
10. [Puis-je installer un logiciel supplémentaire sur l'ordinateur ?](Puis-je installer un logiciel supplémentaire sur l'ordinateur ?)
11. [Comment retourner le matériel lorsque je n'en ai plus l'utilité ?](Comment retourner le matériel lorsque je n'en ai plus l'utilité ?)
12. [Changer de profil](Changer de profil)

## DIRECTEUR ET CHEF-D'ETABLISSEMENT
Je suis paramétré pour répondre aux questions suivantes :
1. [Comment présenter le dispositif TED-i à mes équipes ?](Comment présenter le dispositif TED-i à mes équipes ?)
2. [Quel matériel est mis à disposition dans mon établissement ?](Quel matériel est mis à disposition dans mon établissement ?)
3. [Comment s'effectue le déploiement du dispositif ?](Comment s'effectue le déploiement du dispositif ?)
4. [Quels sont les points de vigilance par rapport à l'utilisation du robot ?](Quels sont les points de vigilance par rapport à l'utilisation du robot ?)
5. [Que faire en cas de problème ?](Que faire en cas de problème ?)
6. [Comment s'effectue le retour du matériel ?](Comment s'effectue le retour du matériel ?)
7. [Changer de profil](Changer de profil)

<!-- Profil ENSEIGNANT -->
## Qu'est ce que le dispositif TED-i ?
Le programme TED-i (Travailler Ensemble à Distance et en Interaction) est un programme national dont l’objectif principal est le maintien du lien social du jeune avec la classe.
* à destination des élèves éloignés de l’école suite à une longue maladie ou un accident de la vie 
* pour une durée supérieure à deux mois 
* sur avis médical, avec l’accord de la famille et de l’établissement scolaire 
* l'accompagnement et le suivi sont réalisés par un référent départemental de la DRNE
​
<iframe title="Présentation Programme TED-i - Enseignants" src="https://tube-institutionnel.apps.education.fr/videos/embed/aef3b540-0f65-42d3-9b34-dc880d5ef54c" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms" width="860" height="484" frameborder="0"></iframe>
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)
​

## Quel matériel est mis à ma disposition en classe ?
Le kit de téléprésence fourni à l'établissement scolaire comprend :
* un robot BEAM, 
* sa base de chargement et son câble de raccordement, 
* une clé 4G (la Sim est activée seulement en cas de besoin). Celle-ci est bridée et ne peut en aucun cas être utilisée sur un ordinateur.

Un visualiseur est disponible sur demande auprès du chargé de déploiement. Lorsqu'il est raccordé au robot, il permet à l'élève de consulter des supports posés sur une table.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d967a237cca04d2abbfa67e66880423e.jpg)

L'élève est quant à lui doté d'un PC et d'une application nommée Beam® lui permettant de se connecter au robot, d'interagir avec ses camarades et professeurs et de piloter le robot dans les salles de classe si besoin.
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)
​

## Comment mettre en fonctionnement le robot BEAM ?
Une fois le robot positionné dans la salle de classe, il suffit d'appuyer sur le bouton latéral pour le mettre en route. L'élève pourra ensuite s'y connecter.
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)
​
## Quelles sont les bonnes pratiques à respecter ?
**Une fois allumé, le robot ne doit pas être déplacé par quelqu'un d'autre que l'élève pilote**, au risque de casser la motorisation.
**Lorsque le robot n'est pas utilisé, il doit être replacé sur sa base de charge**.

>Retrouvez les bonnes pratiques pour prendre soin du robot BEAM [ici](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_prends_soin_de_moi.png).

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_23ad9606720a2929ac5507ccaea03725.png)

**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment mettre en veille le robot BEAM ?
La mise en veille permet de déplacer le robot entre deux cours ou de le déconnecter lorsqu'il n'est pas utilisé. Elle s'effectue via le menu disponible par un appui sur le bouton latéral de la tête du Beam.

Ce [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_mise_en_veille_Beam_Voct2024.pdf) indique la procédure à suivre.

**La mise en veille déconnecte le robot mais ne l'éteint pas. S'il n'est pas utilisé, il doit être maintenu sur son dock de charge afin de ne pas endommager la batterie.**
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Que faire en cas de problème de connexion ?
En cas de soucis de connexion, prendre contact avec le chargé de déploiement dont les coordonnées figurent sur la fiche de communication qui a été remise à l'établissement lors de l'installation du robot. 

**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## L'élève peut-il partager son travail via le robot ?
L'élève a la possibilité de partager une fenêtre sur son ordinateur portable. Il peut ainsi vous montrer son travail via l'écran du BEAM.

**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment utiliser le visualiseur ?
Un visualiseur et son support sont disponibles sur demande auprès du chargé de déploiement. Lorsqu'il est raccordé au robot, l'élève peut consulter des supports posés sur une table ou mieux observer une expérience en sciences.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d967a237cca04d2abbfa67e66880423e.jpg)

Le visualiseur, une fois fourni, ne doit pas être déconnecté du robot. En fin de déploiement, il sera restitué au chargé de déploiement.
**L'élève n'a pas la possibilité de régler la mise au point.** Vous effectuez donc les réglages en vous aidant de l'aperçu affiché sur l'écran du robot.
[Ce tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_utiliser_le_visualiseur_enseignant_Voct2024.pdf) vous guide dans l'utilisation du visualiseur.
La notice du visualiseur comprenant un descriptif des boutons présents sur celui-ci est disponible [ici](https://s3-us-west-1.amazonaws.com/files.ipevo.com/download/doc/v4k/V4K_QSG_French_20180307.pdf).
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment l'élève peut-il me signaler qu'il souhaite prendre la parole ou qu'il a un problème ?
Un fichier contenant des pictogrammes de communication est disponible en téléchargement [ici](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/Pictogrammes-de-communication.zip). Nous recommandons de les installer sur le bureau du PC élève pour faciliter leur utilisation.
Les pictogrammes permettent à l'élève de vous signaler sur l'écran du robot, via un partage d'écran, qu'il :
* souhaite prendre la parole,
* a un souci de compréhension,
* a un problème d'image,
* a un problème de son.
![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_10cd5fa67286258df028b6a840e33004.PNG)

**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur enseignant - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

<!-- Profil ELEVE -->
## Quel matériel est mis à ma disposition ?
Un kit t'est fourni. Il comprend :
* un ordinateur portable et son câble d'alimentation, 
* une souris,
* un micro-casque
* une manette.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_ff979a5406145ae51cc5aac36021a9a8.PNG)

Le PC comporte une application nommée Beam® grâce à laquelle tu peux te connecter au robot, interagir avec tes camarades et professeurs et piloter le robot dans les salles de classe si besoin.

Différents logiciels sont également disponibles sur le PC : traitement de texte, lecteur vidéo, ...
​
**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)
​

## Puis-je utiliser mon ordinateur personnel ou une tablette ?
Il est également possible d'utiliser ton ordinateur portable personnel et d'y installer l'application Beam®, disponible en téléchargement [ici](https://telepresence.awabot.com/support/lien-application-beam/).

L'application mobile Beam Smart Presence est disponible en version bêta. La tablette peut ainsi être utilisée pour échanger via le robot. Elle sera privilégiée dans le premier degré. Son utilisation est moins recommandée dans l'enseignement secondaire où le travail notamment sur des suites bureautiques, sera plus performant sur ordinateur.

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment me connecter à l'application Beam® ?
Le login et le mot de passe de connexion te sont fournis le jour de la remise du kit PC.

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment utiliser l'application Beam® ?
Ce [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_Connexion_robot_BEAM_eleve_Voct2024.pdf) te guidera dans la prise en main de l'application.
​
**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Que faire si je ne parviens pas à me connecter au robot ?
En cas de soucis de connexion, préviens ton établissement et prends contact avec le chargé de déploiement dont les coordonnées figurent sur la fiche de communication remise avec le kit PC. 

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Puis-je consulter un document posé sur une table en classe ?
Un visualiseur peut être fourni à ton établissement scolaire sur simple demande au chargé de déploiement. Une fois connecté au robot BEAM, tu pourras consulter des documents posés sur une table via sa caméra.

Ce [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_utilise_le_visualiseur_eleve_Voct2024.pdf) explique comment se servir du visualiseur.

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment utiliser le visualiseur fixé sur le robot ?
Lorsque le visualiseur est connecté au robot, un bouton apparait dans la barre d'outils à l'intérieur de l'application Beam®. Tu peux alors basculer la vue sur la caméra du visualiseur.

Ce [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_utilise_le_visualiseur_eleve_Voct2024.pdf) détaille la procédure à suivre.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d967a237cca04d2abbfa67e66880423e.jpg)

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)


## Comment utiliser le partage d'écran pour montrer mon travail via le robot ?
Dans l'application Beam®, un bouton de la barre d'outil donne la possibilité de partager l'écran de ton choix via l'écran de la tête du BEAM. Tu peux ainsi montrer rapidement ton travail au professeur ou à tes camarades lors de séances en groupe. 
Ce [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_Partage_ton_ecran_eleve_Voct2024.pdf) explique la démarche à suivre pour partager ton écran.

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment signaler à mon enseignant que je souhaite prendre la parole ou que j'ai un problème ?
Un fichier contenant des pictogrammes de communication est disponible en téléchargement [ici](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/Pictogrammes-de-communication.zip). Nous recommandons de les installer sur le bureau de ton PC pour faciliter leur utilisation.
![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_82281b96cff4199a933bf3692c7a5764.PNG)

Les pictogrammes te permettent de signaler sur l'écran du robot, via un partage d'écran, que :
* tu souhaites prendre la parole,
* tu as un souci de compréhension,
* tu as un problème de son,
* tu as un problème d'image.

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Puis-je installer un logiciel supplémentaire sur l'ordinateur ?
La demande doit être faite au chargé de déploiement qui t'a remis le kit PC. Ses coordonnées sont disponibles sur la fiche de communication. 

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment retourner le matériel lorsque je n'en ai plus l'utilité ?
Lorsque le matériel n'est plus utilisé, il doit être restitué à l'établissement scolaire. Le robot et le kit PC seront ensuite enlevés par un transporteur pour aller aider un autre élève.
**Pensez à bien vérifier que le kit PC est complet avant de le restituer**. 

**As-tu d'autres questions ?**
1. [Oui](Guider l'utilisateur élève - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

<!-- Profil DIRECTEUR ET CHEF ETABLISSEMENT -->
## Comment présenter le dispositif TED-i à mes équipes ?
Des vidéos de présentation du dispositif sont disponibles sur [le site de la DRNE](https://drne.region-academique-bourgogne-franche-comte.fr/presentation-ted-i/).

Le [flyer](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/09/TED-i_DEPLIANT_3VOLETS_VF_19092024.pdf) à destination des enseignants peut également servir de support.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_76edc2a2be068d8f7012f8e98c1e8744.PNG)


Le chargé de déploiement de votre département est disponible pour présenter le dispositif, son fonctionnement et répondre aux questionnements des enseignants.
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Quel matériel est mis à disposition dans mon établissement ?
Le kit de téléprésence fourni à l'établissement scolaire comprend :
* un robot BEAM, 
* sa base de chargement et son câble de raccordement, 
* une clé 4G (la Sim est activée seulement en cas de besoin). Celle-ci est bridée et ne peut en aucun cas être utilisée sur un ordinateur.

Un visualiseur est disponible sur demande auprès du chargé de déploiement. Il permet à l'élève de visualiser des supports posés sur une table lorsqu'il est raccordé au robot.

![](https://)![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d967a237cca04d2abbfa67e66880423e.jpg)


L'élève est quant à lui doté d'un PC et d'une application nommée Beam® lui permettant de se connecter au robot, d'interagir avec ses camarades et professeurs et de piloter le robot dans les salles de classe si besoin.
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment s'effectue le déploiement du dispositif ?
Dès réception de la demande par le SAPAD, le chargé de déploiement effectue une demande de mise à disposition d'un robot auprès de la société Awabot. Vous êtes en parallèle informé que le matériel va être livré dans votre établissement.
Un transporteur est chargé de la livraison du colis (robot+PC élève) dans votre établissement.
Le chargé de déploiement prend ensuite le relai pour assurer la connectivité du robot sur site et apporter des conseils sur son utilisation auprès des utilisateurs (famille et enseignants).
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Quels sont les points de vigilance par rapport à l'utilisation du robot ?
Veiller à **conserver l'emballage** qui sera nécessaire pour la récupération du matériel par le transporteur à la fin du déploiement du dispositif.

**Une fois allumé, le robot ne doit pas être déplacé par quelqu'un d'autre que l'élève pilote**, au risque de casser la motorisation.
**Lorsque le robot n'est pas utilisé, il doit être replacé sur sa base de charge**.

En cas d'inutilisation prolongée (vacances scolaires), le robot peut être éteint et stocké hors de son dock de charge (voir le [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_extinction_stockage_Beam_Voct2024.pdf)) dans son emballage d'origine.
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Que faire en cas de problème ?
En cas de soucis avec le matériel, prendre contact avec le chargé de déploiement dont les coordonnées figurent sur la fiche de communication qui a été remise à l'établissement lors de l'installation du robot. 
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Comment s'effectue le retour du matériel ?
Lorsque la situation de l'élève ne nécessite plus de robot, prévenir le chargé de déploiement. Celui-ci signalera à la société que l'équipement doit être récupéré.
L'établissement se charge de la préparation du colis.
>La famille restitue le kit PC à l'établissement qui procède à son **inventaire** (à l'aide de la fiche de prêt fournie en début de déploiement) avant de ranger l'ensemble du matériel dans l'emballage d'origine. 
>**Le robot doit impérativement être éteint (voir le [tutoriel](https://drne.region-academique-bourgogne-franche-comte.fr/wp-content/uploads/2024/10/TEDi_pas_a_pas_extinction_stockage_Beam_Voct2024.pdf)).** 
>Un **bordereau**, fourni par le chargé de déploiement, sera apposé sur le colis avant son enlèvement par le transporteur.

**Si votre établissement a bénéficié de matériel complémentaire (visualiseur), ce dernier doit être restitué au chargé de déploiement et non emballé avec le robot.**
​
**Avez-vous d'autres questions ?**
1. [Oui](Guider l'utilisateur directeur et chef d'établissement - Oui)
2. [Non](Guider l'utilisateur - Non)
3. [Changer de profil](Changer de profil)

## Guider l'utilisateur enseignant - Oui
Je suis paramétré pour répondre aux questions suivantes :
1. [Qu'est ce que le dispositif TED-i ?](Qu'est ce que le dispositif TED-i ?)
2. [Quel matériel est mis à ma disposition en classe ?](Quel matériel est mis à ma disposition en classe ?)
3. [Quelles sont les bonnes pratiques à respecter ?](Quelles sont les bonnes pratiques à respecter ?)
4. [Comment mettre en fonctionnement le robot BEAM ?](Comment mettre en fonctionnement le robot BEAM ?)
5. [Comment mettre en veille le robot BEAM ?](Comment mettre en veille le robot BEAM ?)
6. [Que faire en cas de problème de connexion ?](Que faire en cas de problème de connexion ?)
7. [L'élève peut-il partager son travail via le robot ?](L'élève peut-il partager son travail via le robot ?)
8. [Comment utiliser le visualiseur ?](Comment utiliser le visualiseur ?)
9. [Comment l'élève peut-il me signaler qu'il souhaite prendre la parole ou qu'il a un problème ?](Comment l'élève peut-il me signaler qu'il souhaite prendre la parole ou qu'il a un problème ?)
10. [Changer de profil](Changer de profil)

## Guider l'utilisateur élève - Oui
Je suis paramétré pour répondre aux questions suivantes :
1. [Quel matériel est mis à ma disposition ?](Quel matériel est mis à ma disposition ?)
2. [Puis-je utiliser mon ordinateur personnel ou une tablette ?](Puis-je utiliser mon ordinateur personnel ou une tablette ?)
3. [Comment me connecter à l'application Beam® ?](Comment me connecter à l'application Beam® ?)
4. [Comment utiliser l'application Beam® ?](Comment utiliser l'application Beam® ?)
5. [Que faire si je ne parviens pas à me connecter au robot ?](Que faire si je ne parviens pas à me connecter au robot ?)
6. [Puis-je consulter un document posé sur une table en classe ?](Puis-je consulter un document posé sur une table en classe ?)
7. [Comment utiliser le visualiseur fixé sur le robot ?](Comment utiliser le visualiseur fixé sur le robot ?)
8. [Comment utiliser le partage d'écran pour montrer mon travail via le robot ?](Comment utiliser le partage d'écran pour montrer mon travail via le robot ?)
9. [Comment signaler à mon enseignant que je souhaite prendre la parole ou que j'ai un problème ?](Comment signaler à mon enseignant que je souhaite prendre la parole ou que j'ai un problème ?) 
10. [Puis-je installer un logiciel supplémentaire sur l'ordinateur ?](Puis-je installer un logiciel supplémentaire sur l'ordinateur ?)
11. [Comment retourner le matériel lorsque je n'en ai plus l'utilité ?](Comment retourner le matériel lorsque je n'en ai plus l'utilité ?)
12. [Changer de profil](Changer de profil)

## Guider l'utilisateur directeur et chef d'établissement - Oui
Je suis paramétré pour répondre aux questions suivantes :
1. [Comment présenter le dispositif TED-i à mes équipes ?](Comment présenter le dispositif Ted-i à mes équipes ?)
2. [Quel matériel est mis à disposition dans mon établissement ?](Quel matériel est mis à disposition dans mon établissement ?)
3. [Comment s'effectue le déploiement du dispositif ?](Comment s'effectue le déploiement du dispositif ?)
4. [Quels sont les points de vigilance par rapport à l'utilisation du robot ?](Quels sont les points de vigilance par rapport à l'utilisation du robot ?)
5. [Que faire en cas de problème ?](Que faire en cas de problème ?)
6. [Comment s'effectue le retour du matériel ?](Comment s'effectue le retour du matériel ?)
7. [Changer de profil](Changer de profil)

## Guider l'utilisateur - Non
Merci de votre visite
L'équipe de la DRNE.
<iframe style="border: none; width: 860px; height: 489px" src="https://grist.incubateur.net/o/drne-bfc-tedi/forms/7gXbe415xpmVQc6jRzsCRj/11"></iframe>

## Changer de profil
Je suis :
1. [ELEVE](ELEVE)
2. [ENSEIGNANT](ENSEIGNANT)
3. [DIRECTEUR ET CHEF D'ETABLISSEMENT](DIRECTEUR ET CHEF-D'ETABLISSEMENT)